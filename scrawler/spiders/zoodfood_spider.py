from scrapy import Spider, Request
from scrapy.selector import Selector

from scrawler.items import RestaurantItem

class ZoodfoodSpider(Spider):
    name = "zoodfood"
    allowed_daomins = ["zoodfood.com"]
    start_urls = [
        "http://zoodfood.com"
    ]

    def parse(self, response):
        for city_list in Selector(response).xpath("//ul[@class='kk-city-list']/li/a"):
            city = city_list.xpath("text()").extract()[0]
            url = city_list.xpath("@href").extract()[0]

            yield Request(response.urljoin(url), callback=self.parse_cities, meta={'city_name': city})

    def parse_cities(self, response):
        for restaurant in Selector(response).xpath("//div[contains(@class, 'kk-city-list')]//a"):
            restaurant_url =  restaurant.xpath("@href").extract()[0]
            restaurant_name = restaurant.xpath("text()").extract()[0]

            meta = {}
            meta['city_name'] = response.meta['city_name']
            meta['restaurant_name'] = restaurant_name
            yield Request(response.urljoin(restaurant_url),
                          callback=self.parse_restaurant,
                          meta=meta)


    def parse_restaurant(self, response):
        item = RestaurantItem()
        # menu = response.xpath("//div[@id='vendor-menu']")
        info = response.xpath("//div[@id='vendor-info']")
        # gallery = response.xpath("//div[@id='vendor-gallery']")
        # review = response.xpath("//div[@id='vendor-review']")
        address_field = info.xpath(
          "//div[@class='tab-content']//div[contains(@class, 'vendor-info-section')]//div//div[@id='vendor-general-info']//div//div[2]//span/text()"
        ).extract()[1]
        g_lat = response.xpath("//meta[@itemprop='latitude']/@content").extract_first()
        g_long = response.xpath("//meta[@itemprop='longitude']/@content").extract_first()
        item['city'] = response.meta['city_name']
        item['url'] = response.url
        item['name'] = response.meta['restaurant_name']
        item['adr'] = address_field
        item['g_lat'] = g_lat
        item['g_long'] = g_long
        yield item
